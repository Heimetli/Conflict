# Konflikte beheben

## Repository erstellen und auf den Server pushen

    C:\ffhs\FTOOP\Git\Conflict>git init
    Initialized empty Git repository in C:/ffhs/FTOOP/Git/Conflict/.git/
    
    C:\ffhs\FTOOP\Git\Conflict>git add .gitignore HelloWorld.java
    
    C:\ffhs\FTOOP\Git\Conflict>git commit -m "Initial commit"
    [master (root-commit) be020e0] Initial commit
     2 files changed, 9 insertions(+)
     create mode 100644 .gitignore
     create mode 100644 HelloWorld.java
    
    C:\ffhs\FTOOP\Git\Conflict>git remote add origin https://gitlab.com/Heimetli/Conflict.git
    
    C:\ffhs\FTOOP\Git\Conflict>git push -u origin master
    Counting objects: 4, done.
    Delta compression using up to 2 threads.
    Compressing objects: 100% (3/3), done.
    Writing objects: 100% (4/4), 390 bytes | 0 bytes/s, done.
    Total 4 (delta 0), reused 0 (delta 0)
    Branch master set up to track remote branch master from origin.
    To https://gitlab.com/Heimetli/Conflict.git
     * [new branch]      master -> master

## File von einem anderen Ort aus ändern

Das kann ich hier leider nicht abbilden

## Das lokale File ebenfalls ändern und das README dazulegen

    C:\ffhs\FTOOP\Git\Conflict>git add README.md
    
    C:\ffhs\FTOOP\Git\Conflict>git commit -a -m "Changed HelloWorld, added README"
    [master 36504cb] Changed HelloWorld, added README
     2 files changed, 1 insertion(+), 1 deletion(-)
     create mode 100644 README.md

## Auf den Server pushen ist nicht mehr möglich weil es einen Konflikt gibt

    C:\ffhs\FTOOP\Git\Conflict>git push
    To https://gitlab.com/Heimetli/Conflict.git
     ! [rejected]        master -> master (fetch first)
    error: failed to push some refs to 'https://gitlab.com/Heimetli/Conflict.git'
    hint: Updates were rejected because the remote contains work that you do
    hint: not have locally. This is usually caused by another repository pushing
    hint: to the same ref. You may want to first integrate the remote changes
    hint: (e.g., 'git pull ...') before pushing again.
    hint: See the 'Note about fast-forwards' in 'git push --help' for details.

## Die Files von Server und lokalem Repository mergen

    C:\ffhs\FTOOP\Git\Conflict>git pull
    remote: Counting objects: 3, done.
    remote: Compressing objects: 100% (3/3), done.
    remote: Total 3 (delta 1), reused 0 (delta 0)
    Unpacking objects: 100% (3/3), done.
    From https://gitlab.com/Heimetli/Conflict
       be020e0..4c1c2e7  master     -> origin/master
    Auto-merging HelloWorld.java
    CONFLICT (content): Merge conflict in HelloWorld.java
    Automatic merge failed; fix conflicts and then commit the result.
    
## HelloWorld.java editieren um die Konflikte zu beheben

Die Konflikte werden durch spezielle Zeilen markiert:

    ++<<<<<<< HEAD
     +      System.out.println( "Hello world!" ) ;
    ++=======
    +       System.out.println( "Hello conflict!" ) ;
    ++>>>>>>> 4c1c2e72c9a23baeb1eb75ff520f52058464ed97

## Das File wieder einchecken

    C:\ffhs\FTOOP\Git\Conflict>git commit -a -m "Resolved conflict"
    [master cc8f360] Resolved conflict

## Und das Repository mit dem Server synchronieren

    C:\ffhs\FTOOP\Git\Conflict>git push
    Counting objects: 6, done.
    Delta compression using up to 2 threads.
    Compressing objects: 100% (5/5), done.
    Writing objects: 100% (6/6), 703 bytes | 0 bytes/s, done.
    Total 6 (delta 1), reused 0 (delta 0)
    To https://gitlab.com/Heimetli/Conflict.git
       4c1c2e7..cc8f360  master -> master





